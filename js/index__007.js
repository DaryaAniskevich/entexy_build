/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/components/blogFilter.js":
/*!*****************************************!*\
  !*** ./src/js/components/blogFilter.js ***!
  \*****************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"blogFilter\": () => (/* binding */ blogFilter)\n/* harmony export */ });\nconst blogFilter = () => {\n  if (window.location.pathname === '/blog.html') {\n    const blogTags = document.querySelector('.blog-tags');\n    const blogTagsButton = blogTags.querySelectorAll('.blog-button');\n\n    const articlesBlock = document.querySelector('.blog-articles');\n    const articlesItems = articlesBlock.querySelectorAll(\n      '.blog-main-content-item',\n    );\n\n    blogTags.addEventListener('click', (e) => {\n      if (!e.target.classList.contains('blog-button')) return false;\n\n      const button = e.target;\n      const buttonTag = button.dataset.tag;\n\n      blogTagsButton.forEach((item) => {\n        item.classList.remove('blog-button_white_active');\n      });\n      button.classList.add('blog-button_white_active');\n\n      articlesItems.forEach((article) => {\n        article.classList.remove('hidden');\n        if (buttonTag === 'all') {\n          article.classList.remove('hidden');\n        } else if (!article.classList.contains(buttonTag)) {\n          article.classList.add('hidden');\n        }\n      });\n    });\n  }\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/blogFilter.js?");

/***/ }),

/***/ "./src/js/components/changeActiveLink.js":
/*!***********************************************!*\
  !*** ./src/js/components/changeActiveLink.js ***!
  \***********************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"changeActiveLink\": () => (/* binding */ changeActiveLink)\n/* harmony export */ });\nconst changeActiveLink = () => {\n  const navItems = document.querySelectorAll('.nav-list-item__link');\n  const location = window.location.href;\n  const servicesButton = document.querySelector(\n    '.nav-list-item__link_services',\n  );\n  const blogButton = document.querySelector('.nav-list-item__link_blog');\n\n  navItems.forEach((item) => {\n    item.classList.remove('nav-list-item__link_active');\n\n    if (item.href === location) {\n      item.classList.add('nav-list-item__link_active');\n    }\n\n    if (location.href === 'https://entexy.com/') {\n      navItems[0].classList.add('nav-list-item__link_active');\n    }\n  });\n  if (\n    location.includes('services-ios') ||\n    location.includes('services-android') ||\n    location.includes('services-cross-platform') ||\n    location.includes('services-erp-crm') ||\n    location.includes('services-frontend') ||\n    location.includes('services-backend') ||\n    location.includes('services-machine-learning') ||\n    location.includes('services-qa-testing')\n  ) {\n    servicesButton.classList.add('nav-list-item__link_active');\n  }\n\n  if (location.includes('blog')) {\n    blogButton.classList.add('nav-list-item__link_active');\n  }\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/changeActiveLink.js?");

/***/ }),

/***/ "./src/js/components/openContactModal.js":
/*!***********************************************!*\
  !*** ./src/js/components/openContactModal.js ***!
  \***********************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"openContactModal\": () => (/* binding */ openContactModal)\n/* harmony export */ });\nconst openContactModal = () => {\n  const openModalButton = document.querySelectorAll('.btn-modal');\n  const modal = document.querySelector('.modal-contact');\n  const modalOverlay = document.querySelector('.modal-overlay-contact');\n  const modalCloseButton = modal.querySelector('.modal-close');\n\n  openModalButton.forEach((button) => {\n    button.addEventListener('click', () => {\n      modal.classList.add('modal--visible');\n      modalOverlay.classList.add('modal-overlay--visible');\n    });\n  });\n\n  modalOverlay.addEventListener('click', (e) => {\n    e.stopPropagation();\n    if (e.target.classList.contains('modal-overlay')) {\n      modal.classList.remove('modal--visible');\n      e.target.classList.remove('modal-overlay--visible');\n    }\n  });\n\n  modalCloseButton.addEventListener('click', () => {\n    modal.classList.remove('modal--visible');\n    modalOverlay.classList.remove('modal-overlay--visible');\n  });\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/openContactModal.js?");

/***/ }),

/***/ "./src/js/components/openLangs.js":
/*!****************************************!*\
  !*** ./src/js/components/openLangs.js ***!
  \****************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"openLangs\": () => (/* binding */ openLangs)\n/* harmony export */ });\nconst openLangs = () => {\n  const openLangButton = document.querySelector('.nav-list-item__link_lang');\n\n  const langMenu = document.querySelector('.header-langs');\n\n  const arrow = document.querySelector('.nav-list-item__link_arrow_lang');\n\n  const openMobileMenuButton = document.querySelector('.header-button-mobile');\n  const mobileMenu = document.querySelector('.header-nav');\n\n  openLangButton.addEventListener('click', () => {\n    langMenu.classList.toggle('header-langs_show');\n    arrow.classList.toggle('nav-list-item__link_arrow_revert');\n  });\n\n  if (window.screen.width <= 1080) {\n    if (mobileMenu.classList.contains('header-nav_show')) {\n      openLangButton.classList.remove('hidden');\n    } else {\n      openLangButton.classList.add('hidden');\n    }\n    openMobileMenuButton.addEventListener('click', () => {\n      if (mobileMenu.classList.contains('header-nav_show')) {\n        setTimeout(() => {\n          openLangButton.classList.add('hidden');\n          langMenu.classList.remove('header-langs_show');\n          arrow.classList.remove('nav-list-item__link_arrow_revert');\n        }, 100);\n      } else {\n        setTimeout(() => {\n          openLangButton.classList.remove('hidden');\n        }, 200);\n      }\n    });\n  }\n\n  const closeAfterClick = (selector) => {\n    selector.addEventListener('click', (e) => {\n      const targetClassList = e.target.classList;\n\n      if (langMenu.classList.contains('header-langs_show')) {\n        targetClassList.forEach((classItem) => {\n          if (\n            !classItem.includes('header') &&\n            !classItem.includes('header-langs')\n          ) {\n            langMenu.classList.remove('header-langs_show');\n            arrow.classList.remove('nav-list-item__link_arrow_revert');\n          }\n        });\n      }\n    });\n  };\n  closeAfterClick(document.querySelector('main'));\n  closeAfterClick(document.querySelector('footer'));\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/openLangs.js?");

/***/ }),

/***/ "./src/js/components/openMobileMenu.js":
/*!*********************************************!*\
  !*** ./src/js/components/openMobileMenu.js ***!
  \*********************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"openMobileMenu\": () => (/* binding */ openMobileMenu)\n/* harmony export */ });\nconst openMobileMenu = () => {\n  const openButton = document.querySelector('.header-button-mobile');\n  const closeButtonImage = document.querySelector(\n    '.header-button-mobile__img_close',\n  );\n  const openButtonImage = document.querySelector(\n    '.header-button-mobile__img_open',\n  );\n  const menu = document.querySelector('.header-nav');\n  const servicesMenu = document.querySelector('.services-menu');\n  const contactButton = document.querySelector('.footer-bottom.btn-modal');\n\n  openButton.addEventListener('click', () => {\n    if (menu.classList.contains('header-nav_show')) {\n      document.documentElement.style = 'overflow-y: auto';\n    } else {\n      document.documentElement.style = 'overflow-y: hidden';\n      contactButton.classList.add('hidden');\n    }\n    menu.classList.toggle('header-nav_show');\n    closeButtonImage.classList.toggle('hidden');\n    openButtonImage.classList.toggle('hidden');\n\n    if (servicesMenu.classList.contains('services-menu_show')) {\n      servicesMenu.classList.remove('services-menu_show');\n      document.documentElement.style = 'overflow-y: unset';\n    }\n  });\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/openMobileMenu.js?");

/***/ }),

/***/ "./src/js/components/openServices.js":
/*!*******************************************!*\
  !*** ./src/js/components/openServices.js ***!
  \*******************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"openServices\": () => (/* binding */ openServices)\n/* harmony export */ });\nconst openServices = () => {\n  const servicesNavMobileButton = document.querySelector(\n    '.nav-list-item__link_mobile ',\n  );\n  const servicesNavDesktopButton = document.querySelector(\n    '.nav-list-item__link_desktop',\n  );\n\n  const servicesMenuDesktop = document.querySelector('.services-menu-desktop');\n  const servicesMenuMobile = document.querySelector('.services-menu-mobile');\n\n  const openLangButton = document.querySelector('.nav-list-item__link_lang');\n  const langMenu = document.querySelector('.header-langs');\n  const langBtnArrow = document.querySelector(\n    '.nav-list-item__link_arrow_lang',\n  );\n\n  const arrow = document.querySelector('.nav-list-item__link_arrow');\n\n  const addListener = (button, menu) => {\n    button.addEventListener('click', () => {\n      menu.classList.toggle('services-menu_show');\n      arrow.classList.toggle('nav-list-item__link_arrow_revert');\n      if (langMenu.classList.contains('header-langs_show')) {\n        langMenu.classList.remove('header-langs_show');\n        langBtnArrow.classList.remove('nav-list-item__link_arrow_revert');\n      }\n    });\n  };\n  addListener(servicesNavMobileButton, servicesMenuMobile);\n  addListener(servicesNavDesktopButton, servicesMenuDesktop);\n\n  const closeAfterClick = (selector) => {\n    selector.addEventListener('click', (e) => {\n      const targetClassList = e.target.classList;\n\n      if (servicesMenuDesktop.classList.contains('services-menu_show')) {\n        targetClassList.forEach((classItem) => {\n          if (\n            !classItem.includes('header') &&\n            !classItem.includes('services-menu')\n          ) {\n            servicesMenuDesktop.classList.remove('services-menu_show');\n            arrow.classList.remove('nav-list-item__link_arrow_revert');\n          }\n        });\n      }\n    });\n  };\n\n  closeAfterClick(document.querySelector('main'));\n  closeAfterClick(document.querySelector('footer'));\n\n  openLangButton.addEventListener('click', () => {\n    if (servicesMenuDesktop.classList.contains('services-menu_show')) {\n      servicesMenuDesktop.classList.remove('services-menu_show');\n      arrow.classList.remove('nav-list-item__link_arrow_revert');\n    }\n  });\n\n  window.addEventListener('resize', () => {\n    servicesMenuDesktop.classList.remove('services-menu_show');\n    servicesMenuMobile.classList.remove('services-menu_show');\n    arrow.classList.remove('nav-list-item__link_arrow_revert');\n  });\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/openServices.js?");

/***/ }),

/***/ "./src/js/components/scrollContactButton.js":
/*!**************************************************!*\
  !*** ./src/js/components/scrollContactButton.js ***!
  \**************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"scrollContactButton\": () => (/* binding */ scrollContactButton)\n/* harmony export */ });\nconst scrollContactButton = () => {\n  const contactButton = document.querySelector('.footer-bottom.btn-modal');\n  // const mobileMenu = document.querySelector('.header-nav');\n\n  if (window.screen.width <= 1080) {\n    window.addEventListener('scroll', () => {\n      contactButton.classList.remove('hidden');\n      const top = window.scrollY;\n      if (top > 30) {\n        contactButton.classList.remove('hidden');\n        // if (mobileMenu.classList.contains('header-nav_show')) {\n        //   contactButton.classList.add('hidden');\n        // }\n      } else {\n        contactButton.classList.add('hidden');\n      }\n    });\n  }\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/scrollContactButton.js?");

/***/ }),

/***/ "./src/js/components/setAccordion.js":
/*!*******************************************!*\
  !*** ./src/js/components/setAccordion.js ***!
  \*******************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"setAccordion\": () => (/* binding */ setAccordion)\n/* harmony export */ });\nconst setAccordion = () => {\n  const accordionOpenButton = document.querySelectorAll(\n    '.accordion-item__header',\n  );\n\n  accordionOpenButton.forEach((button) => {\n    button.addEventListener('click', (e) => {\n      e.target\n        .closest('.accordion-item')\n        .classList.toggle('accordion-item_show');\n    });\n  });\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/setAccordion.js?");

/***/ }),

/***/ "./src/js/components/validateSendForm.js":
/*!***********************************************!*\
  !*** ./src/js/components/validateSendForm.js ***!
  \***********************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"validateSendForm\": () => (/* binding */ validateSendForm)\n/* harmony export */ });\nconst validateSendForm = () => {\n  const contactForm = document.querySelector('#contact-form');\n  const inputs = contactForm.querySelectorAll('input');\n  const textarea = contactForm.querySelector('textarea');\n  const inputFile = contactForm.querySelector('input[type=file]');\n  const inputFileSize = contactForm.querySelector(\n    '.contact-form-file__text_size',\n  );\n  const inputLabelFile = contactForm.querySelector('.input-label_file');\n  const recaptchaButton = contactForm.querySelector('.recaptcha-button');\n  const recaptchaButtonFake = contactForm.querySelector(\n    '.recaptcha-button-fake',\n  );\n  const emailInput = contactForm.querySelector('input[name=email]');\n  const nameInput = contactForm.querySelector('input[name=first_name]');\n  const phoneInput = contactForm.querySelector('input[name=phone_number]');\n\n  const EMAIL_REGEXP =\n    /^(([^<>()[\\]\\\\.,;:\\s@\\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/;\n  const TEL_REGEXP_RU = /^[^а-яё]+$/iu;\n  const TEL_REGEXP_EN = /^[^a-z]+$/iu;\n\n  let fileIsValid = true;\n\n  const setValidInput = (element) => {\n    recaptchaButton.removeAttribute('disabled');\n    element.classList.add('input_valid');\n    element.classList.remove('input_error');\n  };\n\n  const setInvalidInput = (element) => {\n    recaptchaButton.setAttribute('disabled', true);\n    element.classList.remove('input_valid');\n    element.classList.add('input_error');\n  };\n\n  const inputsArray = [...inputs];\n  const formIsInvalid = inputsArray.some((input) =>\n    input.classList.contains('input_error'),\n  );\n\n  const checkIsValid = () => {\n    if (\n      !formIsInvalid &&\n      fileIsValid &&\n      emailInput.value.trim().length !== 0 &&\n      nameInput.value.trim().length !== 0 &&\n      phoneInput.value.trim().length !== 0\n    ) {\n      recaptchaButtonFake.classList.add('hidden');\n      recaptchaButton.classList.remove('hidden');\n    }\n  };\n\n  const checkRequiredInputIsFilled = (input) => {\n    if (!input.value || input.value.trim().length <= 0) {\n      setInvalidInput(input);\n    }\n  };\n\n  recaptchaButtonFake.addEventListener('click', () => {\n    checkRequiredInputIsFilled(emailInput);\n    checkRequiredInputIsFilled(nameInput);\n    checkRequiredInputIsFilled(phoneInput);\n  });\n\n  inputs.forEach((input) => {\n    input.addEventListener('input', (e) => {\n      if (\n        e.target.type !== 'file' &&\n        (e.target.name === 'email' ||\n          e.target.name === 'first_name' ||\n          e.target.name === 'phone_number')\n      ) {\n        if (\n          e.target.value.length > 2 &&\n          e.target.value.length < 150 &&\n          e.target.value.trim().length > 0 &&\n          e.target.value.trim().length < 150\n        ) {\n          setValidInput(e.target);\n        } else {\n          setInvalidInput(e.target);\n        }\n        checkIsValid();\n      }\n\n      if (e.target.type === 'email') {\n        if (EMAIL_REGEXP.test(e.target.value.toLowerCase())) {\n          setValidInput(e.target);\n        } else {\n          setInvalidInput(e.target);\n        }\n        checkIsValid();\n      }\n\n      if (e.target.type === 'tel') {\n        if (\n          TEL_REGEXP_EN.test(e.target.value.toLowerCase()) &&\n          TEL_REGEXP_RU.test(e.target.value.toLowerCase())\n        ) {\n          setValidInput(e.target);\n        } else {\n          setInvalidInput(e.target);\n        }\n        checkIsValid();\n      }\n    });\n  });\n\n  if (textarea) {\n    textarea.addEventListener('change', (e) => {\n      if (e.target.value.length > 3000) {\n        setInvalidInput(e.target);\n      } else {\n        setValidInput(e.target);\n      }\n    });\n  }\n\n  if (inputFile) {\n    inputFile.addEventListener('change', (e) => {\n      const fileSize = e.target?.files[0]?.size\n        ? e.target?.files[0]?.size / 1048576\n        : 0;\n\n      if (fileSize > 10) {\n        inputFileSize.classList.add('contact-form-file__text_error');\n        inputLabelFile.textContent = 'Attach an RFP';\n        fileIsValid = false;\n      } else {\n        inputFileSize.classList.remove('contact-form-file__text_error');\n        const fileName = e.target?.files[0]?.name || '';\n\n        if (fileName) inputLabelFile.textContent = `Attached ${fileName}`;\n        fileIsValid = true;\n      }\n    });\n  }\n  checkIsValid();\n};\n\n\n//# sourceURL=webpack://gulp-package/./src/js/components/validateSendForm.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _utils_is_webp_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/is-webp.js */ \"./src/js/utils/is-webp.js\");\n/* harmony import */ var _components_openServices_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/openServices.js */ \"./src/js/components/openServices.js\");\n/* harmony import */ var _components_changeActiveLink_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/changeActiveLink.js */ \"./src/js/components/changeActiveLink.js\");\n/* harmony import */ var _components_openMobileMenu_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/openMobileMenu.js */ \"./src/js/components/openMobileMenu.js\");\n/* harmony import */ var _components_setAccordion_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/setAccordion.js */ \"./src/js/components/setAccordion.js\");\n/* harmony import */ var _components_scrollContactButton_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/scrollContactButton.js */ \"./src/js/components/scrollContactButton.js\");\n/* harmony import */ var _components_openContactModal_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/openContactModal.js */ \"./src/js/components/openContactModal.js\");\n/* harmony import */ var _components_validateSendForm_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/validateSendForm.js */ \"./src/js/components/validateSendForm.js\");\n/* harmony import */ var _components_blogFilter_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/blogFilter.js */ \"./src/js/components/blogFilter.js\");\n/* harmony import */ var _components_openLangs_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/openLangs.js */ \"./src/js/components/openLangs.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n(0,_utils_is_webp_js__WEBPACK_IMPORTED_MODULE_0__.isWebp)();\r\n(0,_components_openServices_js__WEBPACK_IMPORTED_MODULE_1__.openServices)();\r\n(0,_components_openLangs_js__WEBPACK_IMPORTED_MODULE_9__.openLangs)();\r\n(0,_components_changeActiveLink_js__WEBPACK_IMPORTED_MODULE_2__.changeActiveLink)();\r\n(0,_components_openMobileMenu_js__WEBPACK_IMPORTED_MODULE_3__.openMobileMenu)();\r\n(0,_components_setAccordion_js__WEBPACK_IMPORTED_MODULE_4__.setAccordion)();\r\n(0,_components_scrollContactButton_js__WEBPACK_IMPORTED_MODULE_5__.scrollContactButton)();\r\n(0,_components_openContactModal_js__WEBPACK_IMPORTED_MODULE_6__.openContactModal)();\r\n(0,_components_blogFilter_js__WEBPACK_IMPORTED_MODULE_8__.blogFilter)();\r\n(0,_components_validateSendForm_js__WEBPACK_IMPORTED_MODULE_7__.validateSendForm)();\r\n\n\n//# sourceURL=webpack://gulp-package/./src/js/index.js?");

/***/ }),

/***/ "./src/js/utils/is-webp.js":
/*!*********************************!*\
  !*** ./src/js/utils/is-webp.js ***!
  \*********************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"isWebp\": () => (/* binding */ isWebp)\n/* harmony export */ });\nfunction isWebp() {\r\n  function testWebP(callback) {\r\n    const webP = new Image();\r\n    const onEvent = () => {\r\n      callback(webP.height === 2);\r\n    };\r\n    webP.onload = onEvent;\r\n    webP.onerror = onEvent;\r\n    webP.src =\r\n      // eslint-disable-next-line max-len\r\n      'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';\r\n  }\r\n\r\n  testWebP((support) => {\r\n    const className = support === true ? 'webp' : 'no-webp';\r\n    document.documentElement.classList.add(className);\r\n  });\r\n}\r\n\n\n//# sourceURL=webpack://gulp-package/./src/js/utils/is-webp.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/index.js");
/******/ 	
/******/ })()
;